﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bazar.Database;
using Bazar.Entity;
using Bazar.Services.Code;

namespace Bazar.Services
{
    public class ProductServices
    {
        public static ProductServices Instance{
            get
            {
                if (instance == null)
                {
                    instance = new ProductServices();
                }
                return instance;
            }
        }



        private static ProductServices instance { get; set; }

        private ProductServices()
        {

        }
        BazarDbContext db = new BazarDbContext();
        public void SavePro(Product pro)
        {
            db.Entry(pro.category).State = System.Data.Entity.EntityState.Unchanged;
            db.Products.Add(pro);
            db.SaveChanges();
        }
        public int GetMaxPrice()
        {
            using (var context = new BazarDbContext())
            {
                return (int)(context.Products.Max(x=>x.price));
            }
        }
        public List<Product> searchPro(string searchTerm, int? minPrice, int? maxPrice, int? catId,int? sortVal)
        {
            using (var context = new BazarDbContext())
            {
            var products = context.Products.Include(x=>x.category).ToList();
            if (catId.HasValue)
            {
                    products = products.Where(x => x.category.id == catId.Value).ToList();
             }
            if (!string.IsNullOrEmpty(searchTerm))
            {
                    products = products.Where(x => x.name.ToLower().Contains(searchTerm.ToLower())).ToList();
             }
                if (minPrice.HasValue)
                {
                    products = products.Where(x => x.price >= minPrice.Value).ToList();
                }
                if (maxPrice.HasValue)
                {
                    products = products.Where(x => x.price <= maxPrice.Value).ToList();
                }
                if (sortVal.HasValue)
                {
                    var sortByVal = sortVal.Value;
                    switch ((sortEnums)sortByVal)
                    {
                        case sortEnums.Default:
                            products = products.OrderByDescending(x=>x.id).ToList();
                            break;
                        case sortEnums.popularity:
                            products = products.OrderByDescending(x => x.id).ToList();
                            break;
                        case sortEnums.HighToLow:
                            products = products.OrderByDescending(x => x.price).ToList();
                            break;
                        case sortEnums.lowTohigh:
                            products = products.OrderBy(x => x.price).ToList();
                            break;
                    }
                }
                return products;
            }       
        }
        public List<Product> GetPros()
        {
            using (var context = new BazarDbContext())
            {
                return context.Products.Include(x => x.category).ToList();
            }
        }
        public List<Product> GetLatestPros(int ProNumber)
        {
            using (var context = new BazarDbContext())
            {
                return context.Products.OrderByDescending(x=>x.id).Take(ProNumber).Include(x => x.category).ToList();
            }
        }
        public int GetProCount(string search)
        {
            using (var context = new BazarDbContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(x => x.name.ToLower().Contains(search.ToLower())).Count();
                }
                else
                {
                    return context.Products.Count();
                }
               
            }
        }
        public List<Product> GetPros(List<int> Ids)
        {
            using (var context = new BazarDbContext())
            {
                return context.Products.Where(x=>Ids.Contains(x.id)).ToList();
            }
        }
        public List<Product> GetPros(int pageNo, string search)
        {
            int pagesize = 5;
            using (var context = new BazarDbContext())
            {
                if (!string.IsNullOrEmpty(search))
                {
                    return context.Products.Where(p => p.name != null && p.name.ToLower().Contains(search.ToLower()))
                        .OrderBy(p=>p.id).Skip((pageNo-1)* pagesize).Take(pagesize).Include(x=>x.category).ToList();
                }
                else
                {
                    return context.Products
                        .OrderBy(p => p.id).Skip((pageNo - 1) * pagesize).Take(pagesize).Include(x => x.category).ToList();

                }
            }
        }
        public Product GetPro(int Id)
        {
            var pro = db.Products.Where(x => x.id == Id).Include(x=>x.category).FirstOrDefault();
            return pro;
        }
        public void UpdatePro(Product pro)
        {
            var newPro = db.Products.Find(pro.id);
            db.Entry(newPro).CurrentValues.SetValues(pro);
            //db.Entry(pro.category).State = System.Data.Entity.EntityState.Unchanged;
            db.Entry(newPro).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void DeletePro(Product pro)
        {
            db.Entry(pro).State = EntityState.Deleted;
            db.SaveChanges();
        }
    }
}