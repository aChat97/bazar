﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Bazar.Database;
using Bazar.Entity;


namespace Bazar.Services
{
    public class CategoryServices
    {
        public static CategoryServices Instance {
            get {
                if (instance == null)
                {
                    instance = new CategoryServices();
                }
                return instance;
                }
        }
        private static CategoryServices instance { get; set; }
        private CategoryServices()
        {

        } 
        private BazarDbContext db = new BazarDbContext();
        public void SaveCat(Category cat)
        {
            db.Categories.Add(cat);
            db.SaveChanges();
        }
        public List<Category> GetCats()
        {
            var catli = db.Categories.ToList();
            return catli;
        }
        public List<Category> GetCats(int pageNo)
        {
            int pageSize = 5;
            var catli = db.Categories.OrderBy(x => x.id).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
            return catli;
        }
        public List<Category> GetFeaturedCats()
        {
            var catli = db.Categories.Where(x=>x.IsFeatured&&x.imageUrl!=null).ToList();
            return catli;
        }
        public Category GetCat(int Id)
        {
            var cat = db.Categories.Find(Id);
            return cat;
        }
        public void UpdateCat(Category cat)
        {
            var newCat = db.Categories.Find(cat.id);
            db.Entry(newCat).CurrentValues.SetValues(cat);
            db.Entry(newCat).State = EntityState.Modified;
            db.SaveChanges();
        }
        public void DeleteCat(Category cat)
        {
            db.Entry(cat).State = EntityState.Deleted;
            db.SaveChanges();
        }
    }
}
