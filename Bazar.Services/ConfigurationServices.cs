﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bazar.Entity;
using Bazar.Database;

namespace Bazar.Services
{
    public class ConfigurationServices
    {
        public static ConfigurationServices Instance { get
            {
                if (instance == null)
                {
                    instance = new ConfigurationServices();
                }
                return instance;
            } }
        private static ConfigurationServices instance { get; set; }
        private ConfigurationServices()
        {

        }
        BazarDbContext db = new BazarDbContext();
        public Config getCong(string key)
        {
            var conf = db.Configs.Find(key);
            return conf;
        }
    }
}
