﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Bazar.Entity
{
    public class Config
    {
        [Key]
        public string key { get; set; }
        public string value { get; set; }
    }
}
