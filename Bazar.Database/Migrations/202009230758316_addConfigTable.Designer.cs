﻿// <auto-generated />
namespace Bazar.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class addConfigTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addConfigTable));
        
        string IMigrationMetadata.Id
        {
            get { return "202009230758316_addConfigTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
