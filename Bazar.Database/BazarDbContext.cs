﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Bazar.Entity;
using System.Threading.Tasks;

namespace Bazar.Database
{
    public class BazarDbContext: DbContext
    {
        public BazarDbContext() : base("BazarDb")
        {
        }
           public DbSet<Category>  Categories{ get; set; }
           public DbSet<Product > Products { get; set; }
           public DbSet<Config> Configs { get; set; }
    }
 }
