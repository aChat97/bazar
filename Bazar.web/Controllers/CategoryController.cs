﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bazar.Entity;
using Bazar.Services;
using Bazar.web.Models;

namespace Bazar.web.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult categoryTable(string search ,int? PageNo)
        {
            categoryView ct = new categoryView();
            ct.getAllCats=CategoryServices.Instance.GetCats();
            PageNo = PageNo.HasValue ? PageNo.Value > 0 ?PageNo.Value: 1 : 1;
            if (!string.IsNullOrEmpty(search))
            {
                ct.getAllCats = ct.getAllCats.Where(p => p.name != null && p.name.ToLower().Contains(search.ToLower())).ToList();
            }
            ct.getAllCats= CategoryServices.Instance.GetCats(PageNo.Value);
            ct.PageNo = PageNo.Value;
            return PartialView(ct);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Category cat)
        {
            CategoryServices.Instance.SaveCat(cat);
            return RedirectToAction("categoryTable");
        }
        public ActionResult Edit(int Id)
        {
            var cat = CategoryServices.Instance.GetCat(Id);
            return PartialView(cat);
        }
        [HttpPost]
        public ActionResult Edit(Category cat)
        {
            CategoryServices.Instance.UpdateCat(cat);
            return RedirectToAction("categoryTable");
        }
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            Category cat = CategoryServices.Instance.GetCat(Id);
            CategoryServices.Instance.DeleteCat(cat);
            return RedirectToAction("categoryTable");
        }
    }
}