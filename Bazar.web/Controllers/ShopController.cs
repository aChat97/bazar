﻿using Bazar.Services;
using Bazar.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Bazar.web.Controllers
{
    public class ShopController : Controller
    {
        public ActionResult Index(string searchTerm,int? minPrice,int? maxPrice,int? catId, int? sort)
        {
            shopModelAll Model = new shopModelAll();
            Model.Fcategories = CategoryServices.Instance.GetFeaturedCats();
            Model.maxPrice = ProductServices.Instance.GetMaxPrice();
            Model.Products = ProductServices.Instance.searchPro(searchTerm, minPrice, maxPrice, catId,sort);
            Model.sort = sort;
            return View(Model);
        }
        public ActionResult FilterIndex(string searchTerm, int? minPrice, int? maxPrice, int? catId, int? sort)
        {
            FilterShop Model = new FilterShop();
            Model.products = ProductServices.Instance.searchPro(searchTerm, minPrice, maxPrice, catId, sort);
            return PartialView(Model);
        }
        public ActionResult CheckOut()
        {
            shopModel model = new shopModel();
            var cookiesOfCart = Request.Cookies["cartProducts"];
            if (cookiesOfCart != null)
            {
                var proIds = cookiesOfCart.Value.Split('-').Select(x => int.Parse(x)).ToList();
                model.CartProduct = ProductServices.Instance.GetPros(proIds);
                model.CartProductIds = proIds;
            }
            return View(model);
        }
    }
}