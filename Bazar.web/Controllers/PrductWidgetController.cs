﻿using Bazar.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bazar.Services;

namespace Bazar.web.Controllers
{
    public class PrductWidgetController : Controller
    {
        // GET: PrductWidget
        public ActionResult Products(bool isLatestPro)
        {
            WidgetPro model = new WidgetPro();
            model.IsLatestPRo = isLatestPro;
            if (isLatestPro)
            {
                model.Products = ProductServices.Instance.GetLatestPros(4);
            }
            else
            {
                model.Products = ProductServices.Instance.GetLatestPros(8);
            }
            return PartialView(model);
        }
    }
}