﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Bazar.Entity;
using Bazar.Services;
using Bazar.web.Models;

namespace Bazar.web.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        { 
            return View();
        }
        public ActionResult ProductTable(string search, int?PageNo)
        {
            ProductData model = new ProductData();
            model.searchTerm = search;
            PageNo = PageNo.HasValue ? PageNo.Value > 0 ? PageNo.Value : 1 : 1;
            var totalrecords = ProductServices.Instance.GetProCount(search);
            model.products = ProductServices.Instance.GetPros(PageNo.Value, search);
            if (model.products != null)
            {
                model.pager = new Pager(totalrecords, PageNo, 5);
                return PartialView(model);
            }
            else
            {
            return HttpNotFound();
            }

        }
        [HttpGet]
        public ActionResult Create()
        {
            
            var cat = CategoryServices.Instance.GetCats();
            return PartialView(cat);
        }
        [HttpPost]
        public ActionResult Create(productView pro)
        {

            Product newProduct = new Product();
            newProduct.name = pro.name;
            newProduct.imageUrl = pro.imageUrl;
            newProduct.price = pro.price;
            newProduct.description = pro.description;
            newProduct.IsFeatured = pro.IsFeatured;
            newProduct.category = CategoryServices.Instance.GetCat(pro.category);
            ProductServices.Instance.SavePro(newProduct);
            return RedirectToAction("ProductTable");
        }
        public ActionResult Edit(int Id)
        {
            
            ViewBag.allCat = CategoryServices.Instance.GetCats();
            var pro = ProductServices.Instance.GetPro(Id);
            return PartialView(pro);
        }
        [HttpPost]
        public ActionResult Edit(productView pro)
        {
            
            Product newProduct = new Product();
            newProduct.id = pro.id;
            newProduct.name = pro.name;
            newProduct.imageUrl = pro.imageUrl;
            newProduct.price = pro.price;
            newProduct.description = pro.description;
            newProduct.IsFeatured = pro.IsFeatured;
            newProduct.category = CategoryServices.Instance.GetCat(pro.category);
            ProductServices.Instance.UpdatePro(newProduct);
            return RedirectToAction("ProductTable");
        }
        [HttpPost]
        public ActionResult Delete(int Id)
        {
            ProductServices.Instance.DeletePro(ProductServices.Instance.GetPro(Id));
            return RedirectToAction("ProductTable");
        }
        public ActionResult details(int Id)
        {
            ProDetails pro = new ProDetails();
            pro.product = ProductServices.Instance.GetPro(Id);
            return View(pro);
        }
    }
}