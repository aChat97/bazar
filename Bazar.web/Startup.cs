﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bazar.web.Startup))]
namespace Bazar.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
