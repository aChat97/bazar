﻿using Bazar.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class IndexModel
    {
        public List<Category> Featuredcategories { get; set; }
        public List<Product> FeaturedProducts { get; set; }
    }
}