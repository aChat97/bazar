﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class Pager
    {
        public Pager(int totalItems,int?Page,int pagesize=5)
        {
            if (pagesize == 0) pagesize = 5;
            var totalPages = (int)Math.Ceiling((decimal)totalItems / (decimal)pagesize);
            var currentPage = Page != null ? (int)Page : 1;
            var startPage = currentPage - 5;
            var endPage = currentPage + 4;
            if (startPage <= 0)
            {
                endPage = startPage - 1;
                startPage = 1;
            }
            if (endPage > totalPages) endPage = totalPages;
            if (endPage > 10)
            {
                startPage = endPage - 9;
            }
            TotalItems = totalItems;
            TotalPages = totalPages;
            CurrentPage = currentPage;
            EndPage = endPage;
            StartPage = startPage;
            Pagesize = pagesize;
        }
        public int TotalItems { get; private set; }
        public int CurrentPage { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public int TotalPages { get; private set; }
        public int Pagesize { get; private set; }

    }
}