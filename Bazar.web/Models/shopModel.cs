﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bazar.Entity;

namespace Bazar.web.Models
{
    public class shopModel
    {
        public List<Product> CartProduct { get; set; }
        public List<int> CartProductIds { get; set; }
    }
    public class shopModelAll
    {


        public int maxPrice { get; set; }
        public List<Product> Products { get; set; }
        public List<Category> Fcategories { get; set; }
        public int? sort;
    }
    public class FilterShop{
        public List<Product> products { get; set; }
        }
}