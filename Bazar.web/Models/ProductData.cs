﻿using Bazar.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class ProductData
    {
        public List<Product> products { get; set; }
        public Pager pager { get; set; }
        public string searchTerm { get; set; }
    }
}