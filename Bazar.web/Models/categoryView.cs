﻿using Bazar.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class categoryView
    {
        public List<Category> getAllCats { get; set; }
        public int PageNo { get; set; }
    }
}