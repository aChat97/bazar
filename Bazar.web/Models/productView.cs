﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class productView
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string imageUrl { get; set; }
        public decimal price { get; set; }
        public int category{ get; set; }
        public bool IsFeatured { get; set; }
    }
}