﻿using Bazar.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bazar.web.Models
{
    public class WidgetPro
    {
        public List<Product> Products { get; set; }
        public bool IsLatestPRo { get; set; }
    }
}